from selenium import webdriver  # Libreria del driver de Selenium
from selenium.webdriver.common.keys import Keys


driver = webdriver.Chrome('./chromedriver')  # Esta parte es para identificar donde esta el archivo chromedrive.exe
driver.get("https://duckduckgo.com/")  # para abrir la pagina la cual le estamos haciendo scraping
print(driver.title)
search_bar = driver.find_element_by_name("q")
search_bar.clear()
search_bar.send_keys("Mesopotamia")  # Esto es lo que vamos e buscar en el explotador
search_bar.send_keys(Keys.RETURN)
print(driver.current_url)

elements = driver.find_elements_by_class_name("result__snippet") # Buscar etiqueta de clase

if len(elements) > 0:
    print('results found')

counter = 0

for element in elements:
    counter = counter + 1
    if(element.get_attribute('innerHTML').count('Mesopotamia') > 0):
        print('found Mesopotamia in search #' + str(counter))

print(elements[0].get_attribute('innerHTML'))
